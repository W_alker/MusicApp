# 关于
这是本人毕设的代码仓库，仿网易云音乐的移动端小型Web App。目前仍在开发。<br/>
技术栈采用 `vue2` + `vant`，脚手架 `vue cli`，后端及部署使用`node` + `express`。<br/>
该项目仅作学习和毕业设计作业之用。<br/>
感谢大佬提供的网易云API仓库：https://github.com/Binaryify/NeteaseCloudMusicApi/ 。<br/>

