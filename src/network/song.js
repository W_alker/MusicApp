import { request } from './request'

export function checkSong(sid) {
  return request({
    url: `/check/music?id=${sid}`
  })
}
export function getSongDetail(sid) {
  return request({
    url: `/song/detail?ids=${sid}`
  })
}
export function getSongUrl(sid) {
  return request({
    url: `/song/url?id=${sid}`
  })
}
export function likeSong(sid, like = true) {
  return request({
    url: `/like?id=${sid}&like=${like}&timestamp=${Date.now()}`
  })
}

export const Lyric = {
  get(sid) {
    return request({
      url: `/lyric?id=${sid}`
    })
  }
}
export const Mlog = {
  get(sid) {
    return request({
      url: `/mlog/music/rcmd?songid=${sid}`
    })
  },
  toVideo() {},
  getUrl(mid) {
    return request({
      url: `/mlog/url?id=${mid}`
    })
  }
}

export const comment = {
  /* 
  id : 资源 id, 如歌曲 id,mv id
  type: 数字 , 资源类型 , 对应歌曲 , mv, 专辑 , 歌单 , 电台, 视频对应以下类型
  pageNo:分页参数,第 N 页,默认为 1
  pageSize:分页参数,每页多少条数据,默认 20
  sortType: 排序方式, 1:按推荐排序, 2:按热度排序, 3:按时间排序
  cursor: 当sortType为 3 时且页数不是第一页时需传入,值为上一条数据的 time
 */
  get(sid, sortType = 1, pageNo = 1, cursor) {
    return request({
      url: `/comment/new?id=${sid}&type=0&sortType=${sortType}&pageNo=${pageNo}&${
        cursor ? 'cursor=' + cursor : ''
      }&timestamp=${Date.now()}`
    })
  },
  like(id, cid, t = 1) {
    return request({
      url: `/comment/like?id=${id}&cid=${cid}&type=0&t=${t}`
    })
  },
  publish(id, content) {
    return request({
      url: `/comment?t=1&type=0&id=${id}&content=${content}`
    })
  }
}
